<?php
/*
 * Backend for WMap editor
 * Vitaly Chekryzhev <13hakta@gmail.com>, 2017
 */

$cacti_base = '../..';
$wmdir = $cacti_base . '/plugins/weathermap/configs';
$imgdir = $cacti_base . '/plugins/weathermap/images';

if (is_dir($cacti_base) && file_exists($cacti_base . "/include/config.php")) {
	// include the cacti-config, so we know about the database
	include_once($cacti_base."/include/config.php");

	$config['base_url'] = (isset($config['url_path'])? $config['url_path'] : $cacti_url);
}

$link = NULL;

function connectDB() {
	global $link, $database_default, $database_hostname, $database_username, $database_password;

	$link = new mysqli($database_hostname, $database_username, $database_password, $database_default);

	/* check connection */
	if ($link->connect_errno) {
		printf("Connect failed: %s\n", $link->connect_error);
		exit();
	}
}

$action = $_REQUEST['a'];

switch ($action) {
	case 'list':
		$list = array();

		if ($handle = opendir($wmdir)) {
			while (false !== ($entry = readdir($handle))) {
				if (substr($entry, -5, 5) == '.conf') {
					$entry = str_replace('.conf', '', $entry);
					$list[] = $entry;
				}
			}
		closedir($handle);
	}

	sort($list);
	header('Content-Type: application/json');
	echo json_encode($list);

	break;

	case 'img':
		$list = array();

		$imgArray = array('png', 'jpg', 'jpeg', 'bmp');

		if ($handle = opendir($imgdir)) {
			while (false !== ($entry = readdir($handle))) {
				$file_ext = substr($entry, strrpos($entry, '.') + 1);
				if (in_array($file_ext, $imgArray))
					$list[] = 'images/' . $entry;
			}
		closedir($handle);
	}

	sort($list);
	header('Content-Type: application/json');
	echo json_encode($list);

	break;

	case 'save':
		$name = htmlentities(trim($_POST['name']));
		$name = str_replace(array('.', '/'), '', $name);
		$data = $_POST['data'];

		$filename = $wmdir . '/' . $name . '.conf';
		file_put_contents($filename, $data);
	break;

	case 'get':
		$name = htmlentities(trim($_GET['name']));
		$name = str_replace(array('.', '/'), '', $name);

		$filename = $wmdir . '/' . $name . '.conf';
		readfile($filename);
	break;

	case 'delete':
		$name = htmlentities(trim($_POST['name']));
		$name = str_replace(array('.', '/'), '', $name);

		$filename = $wmdir . '/' . $name . '.conf';

		unlink($filename);
	break;

	case 'dev':
		$filter = htmlentities(trim($_GET['filter']));

		connectDB();

		$query = "SELECT id,hostname,description from host";

		if ($filter != '')
			$query .= ' WHERE description LIKE "' . $filter . '%"';

		$query .= " order by description";

		if ($filter == '')
			$query .= " LIMIT 10";

		$list = array();

		if ($result = $link->query($query)) {
			while ($row = $result->fetch_assoc()) {
				$list[$row['id']] = array($row['description'], $row['hostname']);
			}
		}

		$result->free();

		header('Content-Type: application/json');
		echo json_encode($list);

		$link->close();
	break;

	case 'data':
		$dev = intval(htmlentities(trim($_GET['dev'])));

		if (!$dev) return '';

		connectDB();

		$list = array();

		$query = "select data_template_data.local_data_id, data_template_data.name_cache, data_template_data.data_source_path from data_local,data_template_data,data_input,data_template where data_local.id=data_template_data.local_data_id and data_input.id=data_template_data.data_input_id and data_local.data_template_id=data_template.id and data_local.host_id=$dev order by name_cache;";

		if ($result = $link->query($query)) {
			while ($row = $result->fetch_assoc()) {
				$list[] = array($row['local_data_id'], $row['name_cache'], $row['data_source_path']);
			}
		}

		$result->free();

		header('Content-Type: application/json');
		echo json_encode($list);

		$link->close();
	break;

	case 'graph':
		$data_id = intval(htmlentities(trim($_GET['data'])));

		if (!$data_id) return '';

		connectDB();

		$query = sprintf("select graph_templates_item.local_graph_id FROM graph_templates_item,graph_templates_graph,data_template_rrd where graph_templates_graph.local_graph_id=graph_templates_item.local_graph_id and task_item_id=data_template_rrd.id and local_data_id=%d LIMIT 1;", $data_id);

		if ($result = $link->query($query)) {
			$row = $result->fetch_assoc();
			$list = array($row['local_graph_id']);
		} else $list = 'error';

		$result->free();

		header('Content-Type: application/json');
		echo json_encode($list);

		$link->close();
	break;
}

?>
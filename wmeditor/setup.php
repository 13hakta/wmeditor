<?php
/*
 +-------------------------------------------------------------------------+
 | Copyright (C) 2017-2018 Vitaly Chekryzhev                                    |
 |                                                                         |
 | This program is free software; you can redistribute it and/or           |
 | modify it under the terms of the GNU General Public License             |
 | as published by the Free Software Foundation; either version 2          |
 | of the License, or (at your option) any later version.                  |
 |                                                                         |
 | This program is distributed in the hope that it will be useful,         |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           |
 | GNU General Public License for more details.                            |
 +-------------------------------------------------------------------------+
*/

function plugin_wmeditor_install() {
	api_plugin_register_hook('wmeditor', 'config_arrays', 'wmeditor_add_menuitem', 'setup.php');
}

function wmeditor_check_dependencies() {
	global $plugins;

	if ((!in_array('weathermap', $plugins)) &&
		(db_fetch_cell("SELECT directory FROM plugin_config WHERE directory='weathermap' AND status=1") == "")) {
		return false;
	}

	return true;
}

function wmeditor_version() {
	return plugin_wmeditor_version();
}

function plugin_wmeditor_version() {
	return array(
			'name' 	=> 'wmeditor',
			'version'  => '1.0',
			'longname' => 'Weathermap editor',
			'author'   => 'Vitaly Chekryzhev',
			'homepage' => 'http://13hakta.ru',
			'email'    => '13hakta@gmail.com',
			'url'      => 'http://13hakta.ru/wmeditor.txt'
		);
}

function wmeditor_add_menuitem() {
	global $menu;

	if (isset($config) && array_key_exists('cacti_version', $config)) {
		if (substr($config['cacti_version'], 0, 3) == '0.8') {
			$menu["Management"]['plugins/wmeditor/editor.html'] = 'Weathermap editor';
		}

		if (substr($config['cacti_version'], 0, 2) == "1.") {
			$menu[__("Management")]['plugins/wmeditor/editor.html'] = 'Weathermap editor';
		}
	}
}
